cd backend
mvn clean package
docker build -t backend .
cd ..
cd frontend
npm install
docker build -t frontend .
cd ..
docker-compose up -d
