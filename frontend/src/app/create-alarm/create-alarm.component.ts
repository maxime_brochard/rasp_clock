import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Alarm} from '../model/alarm';
import {RaspclockService} from '../service/rascpclockService';
import * as moment from 'moment';
import {UpdatedAlarm} from '../model/UpdatedAlarm';
import {Audio} from '../model/Audio';

@Component({
    selector: 'app-create-alarm',
    templateUrl: './create-alarm.component.html',
    styleUrls: ['./create-alarm.component.css']
})
export class CreateAlarmComponent implements OnInit {
    @Input() showMe: boolean;
    @Output() cancel = new EventEmitter();
    @Output() sumbittedAlarm = new EventEmitter();
    hour: number;
    min: number;
    status: boolean;
    listOfDays: number[] = [1, 2, 3, 4, 5, 6, 7];
    nameOfDays: string[] = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
    toggleDays: number[] = [];
    listOfAudios: Audio[];
    audioSelectedId: string;


    constructor(private rService: RaspclockService) {
    }

    ngOnInit() {
        this.rService.getAudios().subscribe(
            data => {
                this.listOfAudios = data;
            }
        );
    }

    cancelAlarmCreation() {
        this.cancel.emit();
    }

    createAlarm() {
        const listOfDayNumbers: number[] = this.toggleDays;
        const listOfAudiosSelected: string[] = [this.audioSelectedId];
        const alarmToCreate = new UpdatedAlarm(
            moment(`${this.hour}:${this.min}`, 'HH:mm').format('HH:mm:ss'),
            this.status = true,
            listOfDayNumbers,
            listOfAudiosSelected
        );
        this.rService.createAlarm(alarmToCreate).subscribe(
            alarmdata => {
                if (alarmdata.time) {
                    this.sumbittedAlarm.emit(true);
                    this.cancel.emit();
                } else {
                    this.sumbittedAlarm.emit(false);
                    this.cancel.emit();
                }
            }
        );
    }

    updateDayList(dayNumber: number) {
        if (this.toggleDays.includes(dayNumber)) {
            this.removeDayNumber(this.toggleDays, dayNumber);
        } else {
            this.toggleDays.push(dayNumber);
        }
    }

    removeDayNumber(arr: number[], target: number) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === target) {
                arr.splice(i, 1);
            }
        }
    }

    fromAudioListToIds(audioList: Audio[]): string[] {
        const listToReturn: string[] = [];
        for (const item of audioList) {
            listToReturn.push(item.id);
        }
        return listToReturn;
    }

}
