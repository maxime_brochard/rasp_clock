import {Component, EventEmitter, Input, Output} from '@angular/core';
import {$e} from 'codelyzer/angular/styles/chars';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'raspclock';
    @Input() showCreateAlarm = false;
    @Input() showCreateAlarmButton = true;
    showSubmitValue: boolean;
    submitValue: boolean;
    submitMessage: string;
    @Output() update = new EventEmitter();
    timestamp: EventEmitter<boolean> = new EventEmitter();


    toogleAlarmCreator() {
        this.showCreateAlarm = !this.showCreateAlarm;
        this.showCreateAlarmButton = !this.showCreateAlarmButton;
    }

    updateList() {
        this.timestamp.emit(true);
    }

    toggleSubmitValue($event) {
        if ($event) {
            this.submitMessage = 'L\'event à bien été créé';
        } else {
            this.submitMessage = 'Erreur, l\'event n\'a pas pu être créé';
        }
        this.submitValue = $event;
        this.showSubmitValue = true;
        this.updateList();
    }


}
