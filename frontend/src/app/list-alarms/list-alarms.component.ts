import {Component, EventEmitter, Input, OnChanges, OnInit} from '@angular/core';
import {Alarm} from '../model/alarm';
import {RaspclockService} from '../service/rascpclockService';
import { Observable} from 'rxjs';
import {filter} from 'rxjs/operators';

@Component({
    selector: 'app-list-alarms',
    templateUrl: './list-alarms.component.html',
    styleUrls: ['./list-alarms.component.css']
})
export class ListAlarmsComponent implements OnInit {
    alarms: Observable<Alarm[]>;
    @Input() forceChange: EventEmitter<boolean>;

    constructor(private rService: RaspclockService) {
    }

    ngOnInit() {
        this.updateList();
        this.forceChange.subscribe(() => this.updateList());
    }

    updateList() {
       this.alarms = this.rService.getAlarms();
       // this.alarms = this.alarms.pipe(filter(alarm => alarm.id !== $event));
    }
}
