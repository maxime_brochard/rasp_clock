import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlarmCreatorComponent } from './alarm-creator.component';

describe('AlarmCreatorComponent', () => {
  let component: AlarmCreatorComponent;
  let fixture: ComponentFixture<AlarmCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlarmCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlarmCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
