import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Alarm} from '../model/alarm';
import {UpdatedAlarm} from '../model/UpdatedAlarm';
import {Day} from '../model/Day';
import {Audio} from '../model/Audio';

@Injectable()
export class RaspclockService {
    constructor(private http: HttpClient) {
    }

    getAlarms(): Observable<Alarm[]> {
        return this.http.get<Alarm[]>('http://localhost:8080/alarms');
    }

    updateAlarm(toUpdate: UpdatedAlarm, id: string): Observable<Alarm> {
        return this.http.put<Alarm>(`http://localhost:8080/alarms/${id}`, toUpdate);
    }

    deleteAlarm(id: string): Observable<Alarm> {
        return this.http.delete<Alarm>(`http://localhost:8080/alarms/${id}`);
    }

    createAlarm(updatedAlarm: UpdatedAlarm) {
        return this.http.post<Alarm>(`http://localhost:8080/alarms/`, updatedAlarm);
    }

    getAudios(): Observable<Audio[]> {
        return this.http.get<Audio[]>('http://localhost:8080/audios');
    }
}
