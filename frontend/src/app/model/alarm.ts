import {Day} from './Day';
import {Audio} from './Audio';

export class Alarm {
  id: string;
  time: string;
  status: boolean;
  daysOfWeek: Day[] = [];
  audioAssignements: Audio[] = [];

  constructor(id: string, time: string, status: boolean, daysOfWeek: Day[], audioList: Audio[]) {
    this.id = id;
    this.time = time;
    this.status = status;
    this.daysOfWeek = daysOfWeek;
    this.audioAssignements = audioList;
  }
}
