export class UpdatedAlarm {
    time: string;
    status: boolean;
    daysOfWeek: number[] = [];
    audioAssignements: string[] = [];

    constructor(time: string, status: boolean, daysOfWeek: number[], audioAssignements: string[]) {
        this.time = time;
        this.status = status;
        this.daysOfWeek = daysOfWeek;
        this.audioAssignements = audioAssignements;
    }
}
