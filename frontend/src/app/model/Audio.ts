export class Audio {
    id: string;
    name: string;
    type: string;
    url: string;

    constructor(id: string, name: string, type: string, url: string) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.url = url;
    }
}
