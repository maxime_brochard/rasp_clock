export class Day {
    id: string;
    alarmId: string;
    dow: number;


    constructor(id: string, alarmId: string, dow: number) {
        this.id = id;
        this.alarmId = alarmId;
        this.dow = dow;
    }
}
