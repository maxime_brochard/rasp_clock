import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AlarmComponent } from './alarm/alarm.component';
import { ListAlarmsComponent } from './list-alarms/list-alarms.component';
import { AlarmCreatorComponent } from './alarm-creator/alarm-creator.component';
import {RaspclockService} from './service/rascpclockService';
import {HttpClientModule} from '@angular/common/http';
import { CreateAlarmComponent } from './create-alarm/create-alarm.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AlarmComponent,
    ListAlarmsComponent,
    AlarmCreatorComponent,
    CreateAlarmComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [RaspclockService],
  bootstrap: [AppComponent]
})
export class AppModule { }
