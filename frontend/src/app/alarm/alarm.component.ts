import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Alarm} from '../model/alarm';
import {Day} from '../model/Day';
import {RaspclockService} from '../service/rascpclockService';
import {UpdatedAlarm} from '../model/UpdatedAlarm';
import * as moment from 'moment';
import {Audio} from '../model/Audio';

@Component({
    selector: 'app-alarm',
    templateUrl: './alarm.component.html',
    styleUrls: ['./alarm.component.css']
})
export class AlarmComponent implements OnInit {
    @Input() alarm: Alarm;
    listOfDays: number[] = [1, 2, 3, 4, 5, 6, 7];
    nameOfDays: string[] = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
    toggleDays: number[] = [];
    statusToggle: boolean;
    hourInt: number;
    minuteInt: number;
    @Output() deletedAlarm = new EventEmitter();


    constructor(private rService: RaspclockService) {
    }

    ngOnInit() {
        this.statusToggle = this.alarm.status;
        this.toggleDays = this.getListOfNumberWithListOfDays(this.alarm.daysOfWeek);
        this.hourInt = parseInt(moment(this.alarm.time, 'HH:mm:ss').format('H'), 10);
        this.minuteInt = parseInt(moment(this.alarm.time, 'HH:mm:ss').format('mm'), 10);
    }

    updateStatus(status: boolean) {
        const listOfNumbers: number[] = [];
        for (const day of this.alarm.daysOfWeek) {
            listOfNumbers.push(day.dow);
        }

        let listOfAudio: string[];
        if (this.alarm.audioAssignements.length > 0) {
            listOfAudio = this.fromAudioListToIds(this.alarm.audioAssignements);
        } else {
            listOfAudio = [];
        }

        const updatedAlarm = new UpdatedAlarm(this.alarm.time, status, listOfNumbers, listOfAudio);
        this.updapteAlarm(updatedAlarm, this.alarm.id);
    }

    updateDay(dayNumber: number) {

        let listOfDayNumbers: number[] = this.toggleDays;
        if (listOfDayNumbers.includes(dayNumber)) {
            this.removeDayNumber(listOfDayNumbers, dayNumber);
        } else {
            listOfDayNumbers = this.toggleDays.concat(dayNumber);
        }

        let listOfAudio: string[];
        if (this.alarm.audioAssignements.length > 0) {
            listOfAudio = this.fromAudioListToIds(this.alarm.audioAssignements);
        } else {
            listOfAudio = [];
        }

        const alarmToUpdate = new UpdatedAlarm(this.alarm.time, this.alarm.status, listOfDayNumbers, listOfAudio);
        this.updapteAlarm(alarmToUpdate, this.alarm.id);
    }

    updapteAlarm(alarmToUpdate, id) {
        return this.rService.updateAlarm(alarmToUpdate, id).subscribe(
            alarmData => {
                this.alarm = alarmData;
                this.statusToggle = alarmData.status;
                this.toggleDays = this.getListOfNumberWithListOfDays(alarmData.daysOfWeek);
            });
    }

    deleteAlarm() {
        return this.rService.deleteAlarm(this.alarm.id).subscribe(
            () => {
                this.deletedAlarm.emit(true);
            });
    }

    removeDayNumber(arr: number[], target: number) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === target) {
                arr.splice(i, 1);
            }
        }
    }

    getListOfNumberWithListOfDays(daysList: Day[]): number[] {
        const numberList: number[] = [];
        for (const day of daysList) {
            numberList.push(day.dow);
        }
        return numberList;
    }

    displayDigit(digit: number): string {
        if (digit < 10) {
            return '0' + digit;
        }
        return digit.toString();
    }


    fromAudioListToIds(audioList: Audio[]): string[] {
        const listToReturn: string[] = [];
        for (const item of audioList) {
            listToReturn.push(item.id);
        }
        return listToReturn;
    }
}



