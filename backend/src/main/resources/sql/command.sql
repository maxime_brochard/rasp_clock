drop table if exists audioAssignements;
drop table if exists days;
drop table if exists alarms;
drop table if exists audio;

create table alarms
(
id text primary key,
time text not null,
status boolean not null
);

create table days
(
id text primary key,
alarm_id text REFERENCES alarms(id),
dow int not null
);

create table audio
(
id text primary key,
name text not null,
type text not null,
url text not null
);

create table audioAssignements
(
    alarm_id text REFERENCES alarms (id),
    audio_id text REFERENCES audio (id)
);

insert into alarms values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '07:00:00', true);

insert into days values ('b7127e4c-7483-47ad-922c-30c564948d23', '1a335e9d-c74f-44fc-9c65-9b4ed214ca57', 1);
insert into days values ('b002f34a-2502-4c7c-85f2-358915b9cf10', '1a335e9d-c74f-44fc-9c65-9b4ed214ca57', 2);
insert into days values ('c1d6338e-db18-48c5-b9b1-a310be1c4144', '1a335e9d-c74f-44fc-9c65-9b4ed214ca57', 3);
insert into days values ('b5b1a1c1-627b-4669-b287-42fa22ae7f27', '1a335e9d-c74f-44fc-9c65-9b4ed214ca57', 4);
insert into days values ('6e3a46f0-13d9-4017-a5aa-0bc2bb288166', '1a335e9d-c74f-44fc-9c65-9b4ed214ca57', 5);

insert into audio values ('89c181ca-3e84-4355-9686-2bc839e18fb9','c-lab', 'stream', 'https://www.c-lab.fr/stream/');
insert into audio values ('5fec555b-e2d9-44e9-bf61-82063877f183','nova - BIENTOT_20200212', 'podcast', 'http://www.nova.fr/sites/default/files/podcast-episode/2020-02/BIENTOT_20200212.mp3');

insert into audioassignements values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '89c181ca-3e84-4355-9686-2bc839e18fb9');