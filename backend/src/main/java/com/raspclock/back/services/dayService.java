package com.raspclock.back.services;

import com.raspclock.back.domain.Day;
import com.raspclock.back.domain.NewDay;
import com.raspclock.back.repository.dayRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class dayService {
    public idGenerator idGenerator;
    public dayRepository dayRepo;

    public dayService(com.raspclock.back.services.idGenerator idGenerator, dayRepository dayRepo) {
        this.idGenerator = idGenerator;
        this.dayRepo = dayRepo;
    }

    public List<Day> getAllDays(){
        return (List<Day>) this.dayRepo.findAll();
    }

    public List<Day> getAllDaysByAlarmId(String alarm_id) {
        return (List<Day>) this.dayRepo.findAllById(Collections.singleton(alarm_id));
    }

    public List<Day> fromIntToDays(List<Integer> integerList, String alarm_id){
        List<Day> listOfDays = new ArrayList<>();

        for(Integer intOfDay : integerList){
            // si c'est bien un int dans la liste
            Optional<Integer> maybeInt = Optional.of(intOfDay);
            Optional<NewDay> maybeNewDay = Optional.of(new NewDay(alarm_id, intOfDay));
            maybeNewDay.ifPresent(
                    dayToCreate -> listOfDays.add(new Day(
                            idGenerator.generateNewId(),
                            dayToCreate.getAlarm_id() ,
                            dayToCreate.getDow()))
            );
        }
        return listOfDays;
    }
}
