package com.raspclock.back.services;

import com.raspclock.back.domain.Alarm;
import com.raspclock.back.domain.Audio;
import com.raspclock.back.domain.Day;
import com.raspclock.back.domain.NewAlarm;
import com.raspclock.back.repository.alarmRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//class which deal with the bdd directly by jpa overlayer
@Component
public class alarmService {
    public idGenerator idGenerator;
    public alarmRepository alarmRepo;
    public dayService dayService;
    public audioService audioService;

    public alarmService(idGenerator idGenerator, alarmRepository repo, dayService dayService, audioService audioService) {
        this.idGenerator = idGenerator;
        this.alarmRepo = repo;
        this.dayService = dayService;
        this.audioService = audioService;
    }

    public List<Alarm> getAllAlarms() {
        return (List<Alarm>) this.alarmRepo.findAll(Sort.by(Sort.Direction.ASC, "time"));
    }

    public Optional<Alarm> getAlarmById(String id) {
        return this.alarmRepo.findById(id);
    }

    public Optional<Alarm> deleteAlarm(String id) {
        Optional<Alarm> alarm = this.alarmRepo.findById(id);
        if (alarm.isPresent()) {
            this.alarmRepo.deleteById(id);
            return alarm;
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent alarm");

    }

    public Alarm createAlarm(NewAlarm toCreate) {
        String futureAlarmId = idGenerator.generateNewId();

        List<Day> daysToAdd = this.dayService.fromIntToDays(
                toCreate.getDaysOfWeek(),
                futureAlarmId);

        List<Audio> audioToAdd = new ArrayList<>();
        for (String audioId : toCreate.getAudioAssignements()) {
            Optional<Audio> maybeAudio = this.audioService.getAudioById(audioId);
            maybeAudio.ifPresentOrElse(
                    audioToAdd::add,
                    () -> {
                        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent audio");
                    }
            );
        }


        return this.alarmRepo.save(
                new Alarm(
                        futureAlarmId,
                        toCreate.getTime(),
                        toCreate.getStatus(),
                        daysToAdd,
                        audioToAdd));
    }


    public Optional<Alarm> updateAlarm(String id, NewAlarm userUpdate) {
        List<Day> daysToAdd = this.dayService.fromIntToDays(
                userUpdate.getDaysOfWeek(),
                id);

        List<Audio> audioToAdd = new ArrayList<>();
        for (String audioId : userUpdate.getAudioAssignements()) {
            Optional<Audio> maybeAudio = this.audioService.getAudioById(audioId);
            maybeAudio.ifPresentOrElse(
                    e -> audioToAdd.add(e),
                    () -> {
                        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent audio");
                    }
            );
        }

        Optional<Alarm> alarm = this.alarmRepo.findById(id);
        if (alarm.isPresent()) {
            this.alarmRepo.save(
                    new Alarm(
                            id,
                            userUpdate.getTime(),
                            userUpdate.getStatus(),
                            daysToAdd,
                            audioToAdd));
            return alarm;
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent alarm");
    }
}





