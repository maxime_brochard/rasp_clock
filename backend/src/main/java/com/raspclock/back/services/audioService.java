package com.raspclock.back.services;

import com.raspclock.back.domain.Audio;
import com.raspclock.back.domain.Day;
import com.raspclock.back.repository.audioRepository;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class audioService {
    public idGenerator idGenerator;
    public audioRepository audioRepo;

    public audioService(com.raspclock.back.services.idGenerator idGenerator, audioRepository audioRepository) {
        this.idGenerator = idGenerator;
        this.audioRepo = audioRepository;
    }

    public List<Audio> getAllAudios() {
         return (List<Audio>) this.audioRepo.findAll();
    }

    public Optional<Audio> getAudioById(String id) {
        return this.audioRepo.findById(id);
    }

}
