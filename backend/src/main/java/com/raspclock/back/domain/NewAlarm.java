package com.raspclock.back.domain;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class NewAlarm {
    private LocalTime time;
    private boolean status;
    private List<Integer> daysOfWeek;
    private List<String> audioAssignements;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<String> getAudioAssignements() {
        return audioAssignements;
    }

    public void setAudioAssignements(List<String> audioAssignements) {
        this.audioAssignements = audioAssignements;
    }

    public NewAlarm(LocalTime time, boolean status, List<Integer> daysOfWeek, List<String> audioAssignements) {
        this.time = time;
        this.daysOfWeek = daysOfWeek;
        this.status = status;
        this.audioAssignements = audioAssignements;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public List<Integer> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<Integer> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

}
