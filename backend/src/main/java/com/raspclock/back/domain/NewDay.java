package com.raspclock.back.domain;

import com.raspclock.back.services.idGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NewDay {
    private String alarm_id;
    private int dow;

    public NewDay(String alarm_id, int dow) {
        this.alarm_id = alarm_id;
        this.dow = dow;
    }

    public String getAlarm_id() {
        return alarm_id;
    }

    public void setAlarm_id(String alarm_id) {
        this.alarm_id = alarm_id;
    }

    public int getDow() {
        return dow;
    }

    public void setDow(int dow) {
        this.dow = dow;
    }

}
