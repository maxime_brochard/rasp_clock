package com.raspclock.back.domain;

import com.raspclock.back.services.idGenerator;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "days")
public class Day {
    @Id
    private String id;
    private String alarm_id;
    private int dow;

    public Day(){}


    public Day(String id, String alarm_id, int dow) {
        this.id = id;
        this.alarm_id = alarm_id;
        this.dow = dow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlarm_id() {
        return alarm_id;
    }

    public void setAlarm_id(String alarm_id) {
        this.alarm_id = alarm_id;
    }

    public int getDow() {
        return dow;
    }

    public void setDow(int dow) {
        this.dow = dow;
    }

}
