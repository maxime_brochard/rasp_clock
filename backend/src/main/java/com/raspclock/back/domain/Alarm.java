package com.raspclock.back.domain;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name = "alarms")
public class Alarm {
    @Id
    private String id;
    private LocalTime time;
    private boolean status;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "alarm_id")
    private List<Day> daysOfWeek;

    public List<Audio> getAudioAssignements() {
        return audioAssignements;
    }

    public void setAudioAssignements(List<Audio> audioAssignements) {
        this.audioAssignements = audioAssignements;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "audioassignements",
        joinColumns = @JoinColumn(name = "alarm_id"),
        inverseJoinColumns = @JoinColumn(name = "audio_id"))
    List<Audio> audioAssignements;

    protected Alarm() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Day> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<Day> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }


    public Alarm(String id, LocalTime time, boolean status, List<Day> daysOfWeek, List<Audio> audioAssignements) {
        this.id = id;
        this.time = time;
        this.status = status;
        this.daysOfWeek = daysOfWeek;
        this.audioAssignements = audioAssignements;
    }

    public int fromTimeToInt(String target) {
        LocalTime time = this.time;
        int result = 0;
        if (target.equals("hour")) {
            result = time.getHour();
        } else if (target.equals("minute")) {
            result = time.getMinute();
        }
        return result;
    }
}

