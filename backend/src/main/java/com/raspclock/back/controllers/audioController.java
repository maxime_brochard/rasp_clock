package com.raspclock.back.controllers;

import com.raspclock.back.controllers.representations.DisplayableAudioRepresentation;
import com.raspclock.back.controllers.representations.audioRepresentationMapper;
import com.raspclock.back.controllers.representations.dayRepresentationMapper;
import com.raspclock.back.domain.Audio;
import com.raspclock.back.domain.Day;
import com.raspclock.back.services.audioService;
import com.raspclock.back.services.dayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/audios")
public class audioController {
    public audioService service;
    public audioRepresentationMapper mapper;

    @Autowired
    public audioController(audioService service, audioRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Audio> getAllAudios() {
        return this.service.getAllAudios();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisplayableAudioRepresentation> getAudioById(@PathVariable("id") String id) {
        Optional<Audio> maybeAudio = this.service.getAudioById(id);
        return maybeAudio
                .map(this.mapper::mapToDisplayableAudio)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }
}
