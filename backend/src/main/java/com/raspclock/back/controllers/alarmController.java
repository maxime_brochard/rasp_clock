package com.raspclock.back.controllers;

import com.raspclock.back.controllers.representations.DisplayableAlarmRepresentation;
import com.raspclock.back.controllers.representations.alarmRepresentationMapper;
import com.raspclock.back.domain.Alarm;
import com.raspclock.back.domain.NewAlarm;
import com.raspclock.back.repository.alarmRepository;
import com.raspclock.back.services.alarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/alarms")
public class alarmController {

    public alarmService service;
    private alarmRepresentationMapper mapper;

    @Autowired
    public alarmController(alarmService alarmService, alarmRepresentationMapper alarmRepresentationMapper) {
        this.service = alarmService;
        this.mapper = alarmRepresentationMapper;
    }

    @GetMapping
    public List<Alarm> getAllAlarms() {
        return this.service.getAllAlarms();
    }

    @PostMapping
    public DisplayableAlarmRepresentation createAlarm(@RequestBody NewAlarm body) {
        final Alarm toBeCreatedAlarm;
        if (body != null) {
            toBeCreatedAlarm = this.service.createAlarm(body);
        } else {
            throw new IllegalArgumentException("Please provide a body");
        }
        ;
        return this.mapper.mapToDisplayableAlarm(toBeCreatedAlarm);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DisplayableAlarmRepresentation> deleteAlarm(@PathVariable("id") String id) {
        Optional<Alarm> maybeAlarm = this.service.deleteAlarm(id);
        return maybeAlarm
                .map(this.mapper::mapToDisplayableAlarm)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<DisplayableAlarmRepresentation> updateAlarm(@PathVariable("id") String id, @RequestBody NewAlarm body) {
        Optional<Alarm> maybeAlarm = this.service.updateAlarm(id,body);
        return maybeAlarm
                .map(this.mapper::mapToDisplayableAlarm)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }


    @GetMapping("/{id}")
    public ResponseEntity<DisplayableAlarmRepresentation> getSingleAlarm(@PathVariable("id") String id) {
        Optional<Alarm> alarmToReturn = this.service.getAlarmById(id);
        return alarmToReturn
                .map(this.mapper::mapToDisplayableAlarm)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}

