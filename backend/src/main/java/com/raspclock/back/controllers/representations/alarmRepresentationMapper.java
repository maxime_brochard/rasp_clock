package com.raspclock.back.controllers.representations;

import com.raspclock.back.domain.Alarm;
import org.springframework.stereotype.Component;

@Component
public class alarmRepresentationMapper {
    public DisplayableAlarmRepresentation mapToDisplayableAlarm(Alarm alarm) {

        DisplayableAlarmRepresentation result = new DisplayableAlarmRepresentation();
        result.setId(alarm.getId());
        result.setTime(alarm.getTime());
        result.setStatus(alarm.isStatus());
        result.setDaysOfWeek(alarm.getDaysOfWeek());
        result.setAudioAssignements(alarm.getAudioAssignements());

        return result;
    }
}
