package com.raspclock.back.controllers.representations;

public class DisplayableAudioRepresentation {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
