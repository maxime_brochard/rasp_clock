package com.raspclock.back.controllers;

import com.raspclock.back.controllers.representations.dayRepresentationMapper;
import com.raspclock.back.domain.Day;
import com.raspclock.back.services.dayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/days")
public class dayController {
    public dayService service;
    public dayRepresentationMapper mapper;

    @Autowired
    public dayController(dayService service, dayRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Day> getAllDays(){
        return this.service.getAllDays();
    }

    @GetMapping("/{id}:alarm_id")
    public List<Day> getAllDaysByAlarmId(@RequestParam String alarm_id){
        return this.service.getAllDaysByAlarmId(alarm_id);
    }
}
