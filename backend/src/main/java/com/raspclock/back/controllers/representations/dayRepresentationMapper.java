package com.raspclock.back.controllers.representations;

import com.raspclock.back.controllers.representations.DisplayableAlarmRepresentation;
import com.raspclock.back.domain.Day;
import org.springframework.stereotype.Component;

@Component
public class dayRepresentationMapper {
    public DisplayableDayRepresentation mapToDisplayableAlarm(Day day) {

        DisplayableDayRepresentation result = new DisplayableDayRepresentation();
        result.setId(day.getId());
        result.setAlarm_id(day.getAlarm_id());
        result.setDow(day.getDow());
        return result;
    }
}