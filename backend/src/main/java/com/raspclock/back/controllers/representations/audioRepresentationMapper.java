package com.raspclock.back.controllers.representations;

import com.raspclock.back.domain.Audio;
import org.springframework.stereotype.Component;

@Component
public class audioRepresentationMapper {
    public DisplayableAudioRepresentation mapToDisplayableAudio(Audio audio){
        DisplayableAudioRepresentation result = new DisplayableAudioRepresentation();
        result.setUrl(audio.getUrl());
        return result;
    }
}
