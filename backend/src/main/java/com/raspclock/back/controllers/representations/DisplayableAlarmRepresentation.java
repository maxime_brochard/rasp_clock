package com.raspclock.back.controllers.representations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.raspclock.back.domain.Audio;
import com.raspclock.back.domain.Day;

import java.time.LocalTime;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
//this is a POJO class, without constructor
public class DisplayableAlarmRepresentation {
    private String id;
    private LocalTime time;
    private boolean status;
    private List<Day> daysOfWeek;
    private List<Audio> audioAssignements;

    public List<Audio> getAudioAssignements() {
        return audioAssignements;
    }

    public void setAudioAssignements(List<Audio> audioAssignements) {
        this.audioAssignements = audioAssignements;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }



    public void setId(String id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public void setDaysOfWeek(List<Day> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public String getId() {
        return id;
    }



    public List<Day> getDaysOfWeek() {
        return daysOfWeek;
    }
}
