package com.raspclock.back.repository;

import com.raspclock.back.domain.Audio;
import org.springframework.data.repository.CrudRepository;

public interface audioRepository extends CrudRepository<Audio, String> {
    //interface to deal with JPA bdd overlay
}
