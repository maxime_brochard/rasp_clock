package com.raspclock.back.repository;

import com.raspclock.back.domain.Alarm;
import com.raspclock.back.domain.Day;
import org.springframework.data.repository.CrudRepository;

public interface dayRepository extends CrudRepository<Day, String> {
    //interface to deal with JPA bdd overlay
}