package com.raspclock.back.repository;

import com.raspclock.back.domain.Alarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface alarmRepository extends JpaRepository<Alarm, String> {
    //interface to deal with JPA bdd overlay
}
